﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelScript : MonoBehaviour
{
    private const string DefaultTitle = "Payment portal integration";
    private const string DefaultDescription = "Integrate credit card and PayPal payments";
    private const string DefaultEffort = "20";
    private const string DefaultTypeText = "Development";

    [SerializeField]
    private Button _btnCancel;

    [SerializeField]
    private Button _btnSave;

    [SerializeField]
    private InputField _inputTitle;

    [SerializeField]
    private InputField _inputDescription;

    [SerializeField]
    private InputField _inputEffort;

    [SerializeField]
    private Dropdown _dropdownType;

    [SerializeField]
    private Toggle _toggle;

    // Start is called before the first frame update
    void Start()
    {
        _btnCancel.onClick.AddListener(CancelChanges);
        _toggle.onValueChanged.AddListener(
            delegate {
                ToggleCheckBox();
            }
        );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CancelChanges() {
        _inputTitle.text = DefaultTitle;
        _inputDescription.text = DefaultDescription;
        _inputEffort.text = DefaultEffort;
        _dropdownType.value = _dropdownType.options.FindIndex(option => option.text == DefaultTypeText);
        _toggle.isOn = false;
        _btnSave.gameObject.SetActive(false);
    }

    void ToggleCheckBox() {
        _btnSave.gameObject.SetActive(_toggle.isOn);
    }
}
